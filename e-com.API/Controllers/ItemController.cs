﻿using e_com.Commons.Dto;
using e_com.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace e_com.API.Controllers
{
    [RoutePrefix("api/item")]
    public class ItemController : ApiController
    {
        private IProductBL _bll;

        public ItemController(IProductBL bll)
        {
            _bll = bll;
        }

        [Route("")]
        public IEnumerable<ItemDto> GetItems()
        {
            var list = _bll.GetItem();
            return list;
        }
    }
}
