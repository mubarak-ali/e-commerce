﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e_com.Commons.Dto
{
    [Serializable]
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        internal bool IsNew
        {
            get
            {
                return Id == default(int);
            }
        }
    }
}
