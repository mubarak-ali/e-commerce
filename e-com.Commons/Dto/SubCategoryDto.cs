﻿using System;

namespace e_com.Commons.Dto
{
    [Serializable]
    public class SubCategoryDto
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        internal bool IsNew
        {
            get
            {
                return Id == default(int);
            }
        }
    }
}
