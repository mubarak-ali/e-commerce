﻿using System;

namespace e_com.Commons.Dto
{
    [Serializable]
    public class ItemDto
    {
        public long Id { get; set; }
        public int? CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public string Size { get; set; }
        public int ItemStatusId { get; set; }

        internal bool IsNew
        {
            get
            {
                return Id == default(int);
            }
        }
    }
}
