﻿using DapperExtensions.Mapper;
using e_com.Repository.Configuration;
using e_com.Repository.Entity;

namespace e_com.Repository.Mapping
{
    public class SubCategoryEntityMapper : ClassMapper<SubCategoryEntity>
    {
        public SubCategoryEntityMapper()
        {
            Table(EntityReference.SubCategory.TABLE_NAME);

            Map(s => s.Id).Column(EntityReference.SubCategory.ID).Key(KeyType.Identity);
            Map(s => s.CategoryId).Column(EntityReference.SubCategory.CATEGORY_ID);
            Map(s => s.Code).Column(EntityReference.SubCategory.CODE);
            Map(s => s.Description).Column(EntityReference.SubCategory.DESCRIPTION);
            Map(s => s.Name).Column(EntityReference.SubCategory.NAME);

            AutoMap();
        }
    }
}
