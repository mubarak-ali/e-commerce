﻿using DapperExtensions.Mapper;
using e_com.Repository.Configuration;
using e_com.Repository.Entity;

namespace e_com.Repository.Mapping
{
    class CategoryEntityMapper : ClassMapper<CategoryEntity>
    {
        public CategoryEntityMapper()
        {
            Table(EntityReference.Category.TABLE_NAME);

            Map(c => c.Id).Column(EntityReference.Category.ID).Key(KeyType.Identity);
            Map(c => c.Description).Column(EntityReference.Category.DESCRIPTION);
            Map(c => c.Name).Column(EntityReference.Category.NAME);
            Map(c => c.Type).Column(EntityReference.Category.TYPE);

            AutoMap();
        }
    }
}
