﻿using DapperExtensions.Mapper;
using e_com.Repository.Configuration;
using e_com.Repository.Entity;

namespace e_com.Repository.Mapping
{
    public class ItemEntityMapper : ClassMapper<ItemEntity>
    {
        public ItemEntityMapper()
        {
            Table(EntityReference.Items.TABLE_NAME);

            Map(i => i.Id).Column(EntityReference.Items.ID).Key(KeyType.Identity);
            Map(i => i.CategoryId).Column(EntityReference.Items.CATEGORY_ID);
            Map(i => i.Code).Column(EntityReference.Items.CODE);
            Map(i => i.Color).Column(EntityReference.Items.COLOR);
            Map(i => i.Description).Column(EntityReference.Items.DESCRIPTION);
            Map(i => i.Details).Column(EntityReference.Items.DETAILS);
            Map(i => i.ItemStatusId).Column(EntityReference.Items.STATUS_ID);
            Map(i => i.Name).Column(EntityReference.Items.NAME);
            Map(i => i.Price).Column(EntityReference.Items.PRICE);
            Map(i => i.Size).Column(EntityReference.Items.SIZE);
            Map(i => i.SubCategoryId).Column(EntityReference.Items.SUB_CATEGORY_ID);

            AutoMap();
        }
    }
}
