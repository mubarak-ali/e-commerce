﻿namespace e_com.Repository.Configuration
{
    public class EntityReference
    {

        #region category Table
        public struct Category
        {
            public static string TABLE_NAME = "category";

            public static string ID = "id";
            public static string TYPE = "category_type";
            public static string NAME = "category_name";
            public static string DESCRIPTION = "category_description";
        }
        #endregion

        #region sub_category Table
        public struct SubCategory
        {
            public static string TABLE_NAME = "sub_category";

            public static string ID = "id";
            public static string CATEGORY_ID = "category_id";
            public static string CODE = "sub_cat_code";
            public static string NAME = "sub_cat_name";
            public static string DESCRIPTION = "sub_cat_description";
        }
        #endregion

        #region items Table
        public struct Items
        {
            public static string TABLE_NAME = "item";

            public static string ID = "id";
            public static string CATEGORY_ID = "category_id";
            public static string SUB_CATEGORY_ID = "sub_category_id";
            public static string CODE = "item_code";
            public static string COLOR = "item_color";
            public static string DESCRIPTION = "item_description";
            public static string DETAILS = "item_details";
            public static string NAME = "item_name";
            public static string PRICE = "item_price";
            public static string SIZE = "item_size";
            public static string STATUS_ID = "item_status_id";
        }
        #endregion

    }
}
