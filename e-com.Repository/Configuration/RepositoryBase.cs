﻿using DapperExtensions.Sql;
using Npgsql;
using System.Data;

namespace e_com.Repository.Configuration
{
    public class RepositoryBase
    {
        private IDbConnection _con;

        public RepositoryBase()
        {
            DapperExtensions.DapperExtensions.SqlDialect = new PostgreSqlDialect();
        }

        protected IDbConnection GetConnection(string connectionString)
        {
            if (_con != null)
            {
                if (_con.State == ConnectionState.Closed)
                {
                    _con.Open();
                    return _con;
                }//if
                else
                {
                    return _con;
                }//else
            }//if
            else
            {
                _con = new NpgsqlConnection(connectionString);
                return _con;
            }//else
        }//GetConnection
    }
}
