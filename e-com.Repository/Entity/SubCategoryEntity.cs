﻿namespace e_com.Repository.Entity
{
    public class SubCategoryEntity
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
