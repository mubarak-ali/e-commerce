﻿using e_com.Repository.Interface.Product;
using System;
using e_com.Repository.Entity;
using System.Configuration;
using System.Data;
using e_com.Repository.Configuration;
using DapperExtensions;
using System.Collections.Generic;

namespace e_com.Repository.Implementation.Product
{
    public class ProductRepository : RepositoryBase, IProductRepository
    {
        private IDbConnection _con;

        #region Constructor
        public ProductRepository()
        {
            _con = GetConnection(ConfigurationManager.ConnectionStrings["e-store"].ConnectionString);
        }
        #endregion

        public int InsertCategory(CategoryEntity entity)
        {
            int id = -1;
            _con.Open();
            var trans = _con.BeginTransaction();
            try
            {
                id = _con.Insert(entity);
                trans.Commit();
            }//try
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }//catch
            finally
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
                trans.Dispose();
            }//finally

            return id;
        }//InsertCategory

        public long InsertItem(ItemEntity entity)
        {
            int id = -1;
            _con.Open();
            var trans = _con.BeginTransaction();
            try
            {
                id = _con.Insert(entity);
                trans.Commit();
            }//try
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }//catch
            finally
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
                trans.Dispose();
            }//finally

            return id;
        }//InsertItem

        public int InsertSubCategory(SubCategoryEntity entity)
        {
            int id = -1;
            _con.Open();
            var trans = _con.BeginTransaction();
            try
            {
                id = _con.Insert(entity);
                trans.Commit();
            }//try
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }//catch
            finally
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
                trans.Dispose();
            }//finally

            return id;
        }//InsertSubCategory

        public IEnumerable<ItemEntity> GetItem()
        {
            try
            {
                _con.Open();
                var list = _con.GetList<ItemEntity>();
                return list;
            }//try
            catch (Exception ex)
            {
                _con.Close();
                ex.ToString();
            }//catch
            finally
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
            }//finally

            return null;
        }//GetItem

        public IEnumerable<CategoryEntity> GetCategory()
        {
            try
            {
                _con.Open();
                var list = _con.GetList<CategoryEntity>();
                return list;
            }//try
            catch (Exception ex)
            {
                _con.Close();
                ex.ToString();
            }//catch
            finally
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
            }//finally

            return null;
        }//GetCategory

        public IEnumerable<SubCategoryEntity> GetSubCategory()
        {
            try
            {
                _con.Open();
                var list = _con.GetList<SubCategoryEntity>();
                return list;
            }//try
            catch (Exception ex)
            {
                _con.Close();
                ex.ToString();
            }//catch
            finally
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
            }//finally

            return null;
        }//GetSubCategory

        public IEnumerable<ItemEntity> GetItem(IPredicate predicate)
        {
            try
            {
                _con.Open();
                var list = _con.GetList<ItemEntity>(predicate);
                return list;
            }//try
            catch (Exception ex)
            {
                _con.Close();
                ex.ToString();
            }//catch
            finally
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
            }//finally

            return null;
        }//GetItem

        public IEnumerable<CategoryEntity> GetCategory(IPredicate predicate)
        {
            try
            {
                _con.Open();
                var list = _con.GetList<CategoryEntity>(predicate);
                return list;
            }//try
            catch (Exception ex)
            {
                _con.Close();
                ex.ToString();
            }//catch
            finally
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
            }//finally

            return null;
        }//GetCategory

        public IEnumerable<SubCategoryEntity> GetSubCategory(IPredicate predicate)
        {
            try
            {
                _con.Open();
                var list = _con.GetList<SubCategoryEntity>(predicate);
                return list;
            }//try
            catch (Exception ex)
            {
                _con.Close();
                ex.ToString();
            }//catch
            finally
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
            }//finally

            return null;
        }//GetSubCategory

        #region Dispose functions
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(disposing)
            {
                if (_con.State == ConnectionState.Open)
                    _con.Close();
                _con.Dispose();
            }
        }        
        #endregion
    }
}
