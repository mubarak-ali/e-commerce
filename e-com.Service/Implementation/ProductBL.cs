﻿using e_com.Service.Interface;
using System;
using System.Collections.Generic;
using e_com.Repository.Entity;
using e_com.Repository.Interface.Product;
using e_com.Commons.Dto;
using e_com.Service.Converters;
using DapperExtensions;

namespace e_com.Service.Implementation
{
    public class ProductBL : IProductBL
    {
        #region Variables
        IProductRepository _repo;
        #endregion

        #region Constructor
        public ProductBL(IProductRepository repository)
        {
            _repo = repository;
        }
        #endregion

        public IEnumerable<CategoryDto> GetCategory()
        {
            using (_repo)
            {
                IEnumerable<CategoryDto> list = new List<CategoryDto>();
                var entityList = _repo.GetCategory();
                list = EntityToDto.GetCategoryDtoList(entityList);
                return list;
            }
        }

        public IEnumerable<CategoryDto> GetCategoryById(int id)
        {
            using (_repo)
            {
                IEnumerable<CategoryDto> list = new List<CategoryDto>();
                IPredicate predicate = Predicates.Field<CategoryEntity>(c => c.Id, Operator.Eq, id);
                var entityList = _repo.GetCategory(predicate);
                list = EntityToDto.GetCategoryDtoList(entityList);
                return list;
            }
        }

        public IEnumerable<CategoryDto> GetCategoryByName(string name)
        {
            using (_repo)
            {
                IEnumerable<CategoryDto> list = new List<CategoryDto>();
                IPredicate predicate = Predicates.Field<CategoryEntity>(c => c.Name, Operator.Eq, name);
                var entityList = _repo.GetCategory(predicate);
                list = EntityToDto.GetCategoryDtoList(entityList);
                return list;
            }
        }

        public IEnumerable<ItemDto> GetItem()
        {
            using (_repo)
            {
                IEnumerable<ItemDto> list = new List<ItemDto>();
                var entityList = _repo.GetItem();
                list = EntityToDto.GetItemList(entityList);
                return list;
            }
        }

        public IEnumerable<ItemDto> GetItemByCategoryId(int categoryId)
        {
            using (_repo)
            {
                IEnumerable<ItemDto> list = new List<ItemDto>();
                var predicate = Predicates.Field<ItemEntity>(i => i.CategoryId, Operator.Eq, categoryId);
                var entityList = _repo.GetItem(predicate);
                list = EntityToDto.GetItemList(entityList);
                return list;
            }
        }

        public IEnumerable<ItemDto> GetItemById(long id)
        {
            using (_repo)
            {
                IEnumerable<ItemDto> list = new List<ItemDto>();
                var predicate = Predicates.Field<ItemEntity>(i => i.Id, Operator.Eq, id);
                var entityList = _repo.GetItem(predicate);
                list = EntityToDto.GetItemList(entityList);
                return list;
            }
        }

        public IEnumerable<ItemDto> GetItemByItemCode(string itemCode)
        {
            using (_repo)
            {
                IEnumerable<ItemDto> list = new List<ItemDto>();
                var predicate = Predicates.Field<ItemEntity>(i => i.Code, Operator.Eq, itemCode);
                var entityList = _repo.GetItem(predicate);
                list = EntityToDto.GetItemList(entityList);
                return list;
            }
        }

        public IEnumerable<ItemDto> GetItemByName(string name)
        {
            using (_repo)
            {
                IEnumerable<ItemDto> list = new List<ItemDto>();
                var predicate = Predicates.Field<ItemEntity>(i => i.Name, Operator.Eq, name);
                var entityList = _repo.GetItem(predicate);
                list = EntityToDto.GetItemList(entityList);
                return list;
            }
        }

        public IEnumerable<SubCategoryDto> GetSubCategory()
        {
            using (_repo)
            {
                IEnumerable<SubCategoryDto> list = new List<SubCategoryDto>();
                var entityList = _repo.GetSubCategory();
                list = EntityToDto.GetSubCategoryDtoList(entityList);
                return list;
            }
        }

        public IEnumerable<SubCategoryDto> GetSubCategoryByCode(string code)
        {
            using (_repo)
            {
                IEnumerable<SubCategoryDto> list = new List<SubCategoryDto>();
                var predicate = Predicates.Field<SubCategoryEntity>(s => s.Code, Operator.Eq, code);
                var entityList = _repo.GetSubCategory(predicate);
                list = EntityToDto.GetSubCategoryDtoList(entityList);
                return list;
            }
        }

        public IEnumerable<SubCategoryDto> GetSubCategoryById(int id)
        {
            using (_repo)
            {
                IEnumerable<SubCategoryDto> list = new List<SubCategoryDto>();
                var predicate = Predicates.Field<SubCategoryEntity>(s => s.Id, Operator.Eq, id);
                var entityList = _repo.GetSubCategory(predicate);
                list = EntityToDto.GetSubCategoryDtoList(entityList);
                return list;
            }
        }

        public IEnumerable<SubCategoryDto> GetSubCategoryByName(string name)
        {
            using (_repo)
            { 
                IEnumerable<SubCategoryDto> list = new List<SubCategoryDto>();
                var predicate = Predicates.Field<SubCategoryEntity>(s => s.Name, Operator.Eq, name);
                var entityList = _repo.GetSubCategory(predicate);
                list = EntityToDto.GetSubCategoryDtoList(entityList);
                return list;
            }
        }

        public int InsertCategory(CategoryDto dto)
        {
            using (_repo)
            {
                var id = _repo.InsertCategory(DtoToEntity.GetCategoryEntity(dto));
                return id;
            }
        }

        public long InsertItem(ItemDto dto)
        {
            using (_repo)
            {
                var id = _repo.InsertItem(DtoToEntity.GetItemEntity(dto));
                return id;
            }
        }

        public int InsertSubCategory(SubCategoryDto dto)
        {
            using (_repo)
            {
                var id = _repo.InsertSubCategory(DtoToEntity.GetSubCategoryEntity(dto));
                return id;
            }
        }
    }
}
