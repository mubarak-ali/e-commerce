﻿using e_com.Commons.Dto;
using e_com.Repository.Entity;
using System.Collections.Generic;

namespace e_com.Service.Converters
{
    public class EntityToDto
    {
        #region ItemEntity list to ItemDto list
        public static IEnumerable<ItemDto> GetItemList(IEnumerable<ItemEntity> entityList)
        {
            var dtoList = new List<ItemDto>();
            foreach (var item in entityList ?? new List<ItemEntity>())
            {
                dtoList.Add(new ItemDto
                {
                    CategoryId = item.CategoryId,
                    Code = item.Code,
                    Color = item.Color,
                    Description = item.Description,
                    Details = item.Details,
                    Id = item.Id,
                    ItemStatusId = item.ItemStatusId,
                    Name = item.Name,
                    Price = item.Price,
                    Size = item.Size,
                    SubCategoryId = item.SubCategoryId
                });
            }
            return dtoList;
        }
        #endregion

        #region CategoryEntity list to CategoryDto list
        public static IEnumerable<CategoryDto> GetCategoryDtoList(IEnumerable<CategoryEntity> entityList)
        {
            var dtoList = new List<CategoryDto>();
            foreach (var item in entityList ?? new List<CategoryEntity>())
            {
                dtoList.Add(new CategoryDto
                {
                    Description = item.Description,
                    Id = item.Id,
                    Name = item.Name,
                    Type = item.Type
                });
            }
            return dtoList;
        }
        #endregion

        #region SubCategoryEntity list to SubCategoryDto list
        public static IEnumerable<SubCategoryDto> GetSubCategoryDtoList(IEnumerable<SubCategoryEntity> entityList)
        {
            var dtoList = new List<SubCategoryDto>();
            foreach (var item in entityList ?? new List<SubCategoryEntity>())
            {
                dtoList.Add(new SubCategoryDto
                {
                    CategoryId = item.CategoryId,
                    Code = item.Code,
                    Description = item.Description,
                    Id = item.Id,
                    Name = item.Name
                });
            }

            return dtoList;
        }
        #endregion
    }
}
