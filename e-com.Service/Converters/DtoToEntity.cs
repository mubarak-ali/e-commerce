﻿using e_com.Commons.Dto;
using e_com.Repository.Entity;
using System.Collections.Generic;

namespace e_com.Service.Converters
{
    public class DtoToEntity
    {
        #region ItemDto to Entity
        public static IEnumerable<ItemEntity> GetItemEntity(IEnumerable<ItemDto> dtoList)
        {
            var entityList = new List<ItemEntity>();
            foreach (var item in dtoList)
            {
                entityList.Add(new ItemEntity
                                {
                                    CategoryId = item.CategoryId,
                                    Code = item.Code,
                                    Color = item.Color,
                                    Description = item.Description,
                                    Details = item.Details,
                                    Id = item.Id,
                                    ItemStatusId = item.ItemStatusId,
                                    Name = item.Name,
                                    Price = item.Price,
                                    Size = item.Size,
                                    SubCategoryId = item.SubCategoryId
                                });
            }
            return entityList;
        }

        public static ItemEntity GetItemEntity(ItemDto dto)
        {
            var entity = new ItemEntity();
            if(dto != null)
            {
                entity.CategoryId = dto.CategoryId;
                entity.Code = dto.Code;
                entity.Color = dto.Color;
                entity.Description = dto.Description;
                entity.Details = dto.Details;
                entity.Id = dto.Id;
                entity.ItemStatusId = dto.ItemStatusId;
                entity.Name = dto.Name;
                entity.Price = dto.Price;
                entity.Size = dto.Size;
                entity.SubCategoryId = dto.SubCategoryId;
            }//if

            return entity;
        }
        #endregion

        #region CategoryDto to Entity
        public static CategoryEntity GetCategoryEntity(CategoryDto dto)
        {
            var entity = new CategoryEntity();
            if(dto != null)
            {
                entity.Description = dto.Description;
                entity.Id = dto.Id;
                entity.Name = dto.Name;
                entity.Type = dto.Type;
            }//if

            return entity;
        }
        #endregion

        #region SubCategoryDto to Entity
        public static SubCategoryEntity GetSubCategoryEntity(SubCategoryDto dto)
        {
            var entity = new SubCategoryEntity();
            if(dto != null)
            {
                entity.CategoryId = dto.CategoryId;
                entity.Code = dto.Code;
                entity.Description = dto.Description;
                entity.Id = dto.Id;
                entity.Name = dto.Name;
            }//if

            return entity;
        }
        #endregion

    }
}
