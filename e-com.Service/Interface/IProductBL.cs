﻿using e_com.Commons.Dto;
using e_com.Repository.Entity;
using System.Collections.Generic;

namespace e_com.Service.Interface
{
    public interface IProductBL
    {
        int InsertCategory(CategoryDto entity);

        int InsertSubCategory(SubCategoryDto entity);

        long InsertItem(ItemDto entity);

        IEnumerable<ItemDto> GetItem();

        IEnumerable<CategoryDto> GetCategory();

        IEnumerable<SubCategoryDto> GetSubCategory();

        IEnumerable<ItemDto> GetItemById(long id);

        IEnumerable<ItemDto> GetItemByName(string name);

        IEnumerable<ItemDto> GetItemByCategoryId(int categoryId);

        IEnumerable<ItemDto> GetItemByItemCode(string itemCode);

        IEnumerable<CategoryDto> GetCategoryById(int id);

        IEnumerable<CategoryDto> GetCategoryByName(string name);

        IEnumerable<SubCategoryDto> GetSubCategoryById(int id);

        IEnumerable<SubCategoryDto> GetSubCategoryByName(string name);

        IEnumerable<SubCategoryDto> GetSubCategoryByCode(string code);

    }
}
