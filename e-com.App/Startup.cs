﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(e_com.App.Startup))]
namespace e_com.App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
